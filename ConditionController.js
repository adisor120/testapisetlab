
const pool = require('./Connect')
const getCondition = (request, response) => {
    pool.query('SELECT "UID" as uid, "LabCode" as labCode, "Condition" condition, "msResultUID" as "msResultUID","Gender" as gender, "ActiveDateFrom" as "activeDateFrom", "ActiveDateTo" as "activeDateTo","StatusFlag" as "statusFlag", agefrom, ageto FROM "msCondition" ORDER BY "UID" DESC', (error, results) => {
        if (error) {
       //     throw error
        }
        //console.log(results);
        response.status(200).json(results.rows);
    })
}
const getConditionByCode = (request, response) => {
    const Code = request.params.Code//parseInt(
    pool.query('SELECT "UID" as uid, "LabCode" as labCode, "Condition" condition, "msResultUID" as "msResultUID","Gender" as gender, "ActiveDateFrom" as "activeDateFrom", "ActiveDateTo" as "activeDateTo","StatusFlag" as "statusFlag", agefrom, ageto FROM "msCondition" WHERE "LabCode" = $1', [Code], (error, results) => {
        if (error) {
      //      throw error
        }
        response.status(200).json(results.rows);
    })
}
const createCondition = (request, response) => {
    const { labCode, condition, msResultUID, gender, activeDateFrom, activeDateTo,
        statusFlag, agefrom, ageto } = request.body
    pool.query('INSERT INTO "msCondition" ("LabCode", "Condition","msResultUID","Gender","ActiveDateFrom","ActiveDateTo","StatusFlag","agefrom","ageto") VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9) RETURNING "UID"',
        [labCode, condition, msResultUID, gender, activeDateFrom, activeDateTo, statusFlag, agefrom, ageto], (error, results) => {
            if (error) {
        //        throw error
            }
            if(results.rows[0]["UID"]!='')
            {
                const id = parseInt(results.rows[0]["UID"])
                pool.query(
                    'UPDATE "msCondition" SET "msResultUID"=$1 WHERE "UID" = $1',[id],
                    (error, results) => {
                        if (error) {
                     //       throw error
                        }
            })
        }
        response.send("บันทึกเรียบร้อยแล้วครับ");
        })
}
const updateCondition = (request, response) => {
    const id = parseInt(request.params.uid)
    const { labCode, condition, msResultUID, gender, activeDateFrom, activeDateTo, statusFlag, agefrom, ageto, uid } = request.body
    pool.query(
        'UPDATE "msCondition" SET "LabCode"=$1, "Condition"=$2, "msResultUID"=$3, "Gender"=$4, "ActiveDateFrom"=$5, "ActiveDateTo"=$6,"StatusFlag"=$7,"agefrom"=$8, "ageto"=$9 WHERE "UID" = $10',
        [labCode, condition, msResultUID, gender, activeDateFrom, activeDateTo, statusFlag, agefrom, ageto, uid],
        (error, results) => {
            if (error) {
            //    throw error
            }
            response.send("แก้ไขเรียบร้อยแล้วครับ");
        }
    )
}
const deleteCondition = (request, response) => {
    const id = parseInt(request.params.uid)
    pool.query('DELETE FROM "msCondition" WHERE "UID" = $1',[id], (error, results) => {
        if (error) {
       //     throw error
        }
        //console.log(id)
        response.send("ลบข้อมูลเรียบร้อยแล้วครับ");
    })
}

module.exports = {
    getCondition,
    getConditionByCode,
    createCondition,
    updateCondition,
    deleteCondition,
}