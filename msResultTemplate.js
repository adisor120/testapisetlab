const pool = require('./Connect')

const getResultTemplate = (request, response) => {
    pool.query('SELECT * FROM "msResultTemplate" ORDER BY "UID" DESC limit 5', (error, results) => {
        if (error) {
        //    throw error
        }
        response.status(200).json(results.rows);
    })
}
const getResultTemplateByUID = (request, response) => {
    const uid = parseInt(request.params.UID)//parseInt(
    pool.query('SELECT * FROM "msResultTemplate" WHERE "UID" = $1', [uid], (error, results) => {
        if (error) {
         //   throw error
        }
        response.status(200).json(results.rows);
    })
}
const getResultTemplateByname = (request, response) => {
    const name = "%" + request.params.name + "%"//parseInt(
    pool.query('SELECT * FROM "msResultTemplate" WHERE lower("Description") like lower($1) or lower("Category") like lower($1)  limit 20 ', [name], (error, results) => {
        if (error) {
      //      throw error
        }
        response.status(200).json(results.rows);
    })
}
const postResultTemplateByDescription = (request, response) => {
    const {Description,Category} = request.body
    var strDescription="%"+Description+"%";
    pool.query('SELECT * FROM "msResultTemplate" WHERE (lower("Category") = lower($2) and lower("Description") like lower($1))   limit 20 ', [strDescription,Category], (error, results) => {
        if (error) {
            //throw error
            response.status(404).send("Not Found");
        }
        response.status(200).json(results.rows);
    })
}
const getCategorybyResultTemplate = (request, response) => {
    pool.query('SELECT  distinct("Category") FROM public."msResultTemplate" where "Category" is not null ORDER BY "Category" DESC', (error, results) => {
        if (error) {
            response.status(404).send("Not Found");
        }
        response.status(200).json(results.rows);
    })
}
const createResultTemplate = (request, response) => {
    const { UID, Description, Category, Language, StatusFlag, CUser, CWhen, MUser, MWhen, sortseq, bilingual } = request.body
    pool.query('INSERT INTO "msResultTemplate" ("Description", "Category", "Language", "StatusFlag","CUser","CWhen" ,"MUser","MWhen", sortseq, bilingual) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10) RETURNING "UID"',
        [Description, Category, Language, StatusFlag, CUser, CWhen, MUser, MWhen, sortseq, bilingual], (error, results) => {
            if (error) {
                console.log(error)
           //     throw error
            }
       
            response.status(200).json(results.rows);
            
        })
}
const updateResultTemplate = (request, response) => {
    const uid = parseInt(request.params.uid)
    const { UID, Description, Category, Language, StatusFlag, CUser, CWhen, MUser, MWhen, sortseq, bilingual } = request.body
    pool.query('UPDATE "msResultTemplate" SET "Description"=$2,"Category"=$3,"Language"=$4,"StatusFlag"=$5,"CUser"=$6,"CWhen"=$7,"MUser"=$8,"MWhen"=$9,sortseq=$10,bilingual=$11 WHERE "UID" = $1',
        [uid, Description, Category, Language, StatusFlag, CUser, CWhen, MUser, MWhen, sortseq, bilingual],
        (error, results) => {
            if (error) {
                response.status(404);
            }
            //else {
                response.status(200).json(results.rows);
            //}
        }
    )
}
const deleteResultTemplate = (request, response) => {
    const id = parseInt(request.params.uid)
    pool.query('DELETE FROM "msResultTemplate" WHERE "UID" = $1', [id], (error, results) => {
        if (error) {
            response.status(404);
        }
        //console.log(id)
        response.status(200).json({"mess":2,"Description":"ลบแล้วครับ"});
    })
}
module.exports = {
    getResultTemplate,
    getResultTemplateByUID,
    createResultTemplate,
    updateResultTemplate,
    deleteResultTemplate,
    getCategorybyResultTemplate,
    getResultTemplateByname,
    postResultTemplateByDescription
}