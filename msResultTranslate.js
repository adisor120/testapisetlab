const pool = require('./Connect')

const getResultTranslate = (request, response) => {
    pool.query('SELECT "UID" as uid, "LabDesc" as "labDesc","TranslateResult" as "translateResult","Recommend" as "recommend","NormalStatus" as "normalStatus","StatusFlag" as "statusFlag","TranslateResult_EN" as "translateResult_EN","Recommend_EN" as "recommend_EN" FROM "msResultTranslate" ORDER BY "UID" DESC limit 5', (error, results) => {
        if (error) {
        //    throw error
        }
        response.status(200).json(results.rows);
    })
}
const getResultTranslateByUID = (request, response) => {
    const uid = parseInt(request.params.UID)//parseInt(
    pool.query('SELECT "UID" as uid, "LabDesc" as "labDesc","TranslateResult" as "translateResult","Recommend" as "recommend","NormalStatus" as "normalStatus","StatusFlag" as "statusFlag","TranslateResult_EN" as "translateResult_EN","Recommend_EN" as "recommend_EN" FROM "msResultTranslate" WHERE "UID" = $1 limit 20', [uid], (error, results) => {
        if (error) {
           // throw error
        }
        response.status(200).json(results.rows);
    })
}
const getResultTranslateByName = (request, response) => {
    const Name = "%"+request.params.Name+"%"//parseInt(
    pool.query('SELECT "UID" as uid, "LabDesc" as "labDesc","TranslateResult" as "translateResult","Recommend" as "recommend","NormalStatus" as "normalStatus","StatusFlag" as "statusFlag","TranslateResult_EN" as "translateResult_EN","Recommend_EN" as "recommend_EN" FROM "msResultTranslate" WHERE lower("LabDesc") like lower($1) or lower("TranslateResult") like lower($1) limit 20', [Name], (error, results) => {
        if (error) {
          //  throw error
        }
        response.status(200).json(results.rows);
    })
}

const createResultTranslate = (request, response) => {
    const { uid, labDesc, translateResult, recommend, normalStatus, statusFlag,
        translateResult_EN, recommend_EN } = request.body
    pool.query('INSERT INTO "msResultTranslate" ("UID", "LabDesc","TranslateResult","Recommend","NormalStatus","StatusFlag","TranslateResult_EN","Recommend_EN") VALUES ($1,$2,$3,$4,$5,$6,$7,$8) RETURNING "UID"',
        [uid, labDesc, translateResult, recommend, normalStatus, statusFlag,translateResult_EN, recommend_EN], (error, results) => {
            if (error) {
           //     throw error
            }
            response.status(200).json(results.rows);
        })
}
const updateResultTranslate = (request, response) => {
    const id = parseInt(request.params.uid)
    const { uid, labDesc, translateResult, recommend, normalStatus, statusFlag,
        translateResult_EN, recommend_EN } = request.body
    pool.query(
        'UPDATE "msResultTranslate" SET "LabDesc"=$2,"TranslateResult"=$3,"Recommend"=$4,"NormalStatus"=$5,"StatusFlag"=$6,"TranslateResult_EN"=$7,"Recommend_EN"=$8 WHERE "UID" = $1',
        [uid, labDesc, translateResult, recommend, normalStatus, statusFlag,
            translateResult_EN, recommend_EN],
        (error, results) => {
            if (error) {
           //     throw error
            }
            response.status(200).json(results.rows);
        }
    )
}
const deleteResultTranslate = (request, response) => {
    const id = parseInt(request.params.uid)
    pool.query('DELETE FROM "msResultTranslate" WHERE "UID" = $1',[id], (error, results) => {
        if (error) {
        //    throw error
        }
        //console.log(id)
        response.status(200).json({"mess":2,"Description":"ลบแล้วครับ"});
    })
}
module.exports = {
    getResultTranslate,
    getResultTranslateByUID,
    createResultTranslate,
    updateResultTranslate,
    deleteResultTranslate,
    getResultTranslateByName
}
