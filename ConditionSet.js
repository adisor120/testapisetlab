const pool = require('./Connect')


const getCondition = (request, response) => {

    var sql = 'SELECT c."UID", "LabCode", "Condition", "msResultUID", "Gender", "ActiveDateFrom", "ActiveDateTo", c."StatusFlag", agefrom, ageto ' +
        ' ,t."UID", t."LabDesc", t."TranslateResult", t."Recommend", t."NormalStatus", t."StatusFlag", t."TranslateResult_EN", t."Recommend_EN"' +
        '  FROM "msCondition" as c ' +
        'join "msResultTranslate" as t on  c."msResultUID"=t."UID" ORDER BY c."UID" DESC';
 
    pool.query(sql, (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found")
        }
        //   console.log(results);
        response.status(200).json(results.rows);
    })
}

const getConditionbySearch = (request, response) => {
    const search = request.params.search;
    var sql = 'SELECT c."UID", "LabCode", "Condition", "msResultUID", "Gender", "ActiveDateFrom", "ActiveDateTo", c."StatusFlag", agefrom, ageto ' +
        ' ,t."UID", t."LabDesc", t."TranslateResult", t."Recommend", t."NormalStatus", t."StatusFlag", t."TranslateResult_EN", t."Recommend_EN"' +
        '  FROM "msCondition" as c ' +
        'join "msResultTranslate" as t on  c."msResultUID"=t."UID" where  lower("LabCode")  = lower($1)  ORDER BY c."UID" DESC';

    pool.query(sql, [search], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found")
        }
        response.status(200).json(results.rows);
    })
}

const postCondition = (request, response) => {
    try {
        const {
            UID,
            Condition,
            Gender,
            LabCode,
            TranslateResult,
            Recommend,
            NormalStatus,
            TranslateResult_EN,
            Recommend_EN,
            agefrom,
            ageto
        } = request.body;
        console.log(request.body)
        var sql = 'INSERT INTO "msCondition" ("LabCode", "Condition","Gender","agefrom","ageto") VALUES ($1,$2,$3,$4,$5) RETURNING "UID"';
        pool.query(sql, [LabCode, Condition, Gender, agefrom, ageto], (error, results) => {
            if (error) {
                console.log(error)
                response.status(404).send("Not Found")
            }
console.log(results.rows)
            if (results.rows[0]["UID"] != '') {

                const id = parseInt(results.rows[0]["UID"])
                pool.query('UPDATE "msCondition" SET "msResultUID"=$1 WHERE "UID" = $1', [id],
                    (error, results) => {
                        if (error) {
                            console.log(error)
                            response.status(404).send("Not Found")
                        }
                    })


                var Translate = 'INSERT INTO "msResultTranslate"("UID", "TranslateResult", "Recommend", "NormalStatus",  "TranslateResult_EN", "Recommend_EN") ' +
                    ' VALUES ($1, $2, $3, $4, $5, $6)';
                pool.query(Translate, [id, TranslateResult, Recommend, NormalStatus, TranslateResult_EN, Recommend_EN],
                    (error, results) => {
                        if (error) {
                            console.log(error)
                            response.status(404).send("Not Found");
                        }
                        response.status(200).json(results.rows);
                    })
            }
        })
            
    } catch (error) {
        console.log(error)
    }

}


// const putCondition = (request, response) => {
//     const id = parseInt(request.params.UID)

//     const { LabCode, Condition, msResultUID, Gender, ActiveDateFrom, ActiveDateTo, StatusFlag, agefrom, ageto, UID } = request.body
//    try {
//     pool.query(
//         'UPDATE "msCondition" SET "LabCode"=$1, "Condition"=$2, "msResultUID"=$3, "Gender"=$4, "ActiveDateFrom"=$5, "ActiveDateTo"=$6,"StatusFlag"=$7,"agefrom"=$8, "ageto"=$9 WHERE "UID" = $10  RETURNING *',
//         [LabCode, Condition, msResultUID, Gender, ActiveDateFrom, ActiveDateTo, StatusFlag, agefrom, ageto, UID],
//         (error, results) => {
//             if (error) {

//                 if (error) 
//                 {
//                     console.log('Update msCondition'+error) 
//                     response.status(404).send("Not Found")
//             } 
//             } 
//             response.status(200).json(results.rows);
//         })
//    } catch (error) {
//     console.log(error)
//     response.status(404).send("Not Found")
//    }
// }


const putCondition = (request, response) => {
    try {
        const {
            UID,
            Condition,
            Gender,
            LabCode,
            TranslateResult,
            Recommend,
            NormalStatus,
            TranslateResult_EN,
            Recommend_EN,
            agefrom,
            ageto
        } = request.body;
        var sql = 'UPDATE "msCondition" SET "LabCode"=$1, "Condition"=$2,"Gender"=$3,"agefrom"=$4,"ageto"=$5 WHERE "UID" = $6  RETURNING *';
        pool.query(sql, [LabCode, Condition, Gender, agefrom, ageto, UID], (error, results) => {
            if (error) {
                console.log(error)
                response.status(404).send("Not Found")
            }
            if (UID != '') {
                const TranslateUID = 'SELECT "UID"  FROM "msResultTranslate"  WHERE "UID" = $1 '
                const resUID = pool.query(TranslateUID, [UID]);

                if (resUID == '') {
                    var Translate = 'INSERT INTO "msResultTranslate"("UID", "TranslateResult", "Recommend", "NormalStatus",  "TranslateResult_EN", "Recommend_EN") ' +
                        ' VALUES ($1, $2, $3, $4, $5, $6)';
                    pool.query(Translate, [UID, TranslateResult, Recommend, NormalStatus, TranslateResult_EN, Recommend_EN],
                        (error, results) => {
                            if (error) {
                                console.log(error)
                                response.status(404).send("Not Found")
                            }
                        })

                } else {
                    var Translate = 'UPDATE  "msResultTranslate" SET "TranslateResult"=$1, "Recommend"=$2, "NormalStatus"=$3,  "TranslateResult_EN"=$4, "Recommend_EN"=$5  WHERE "UID" = $6';
                    pool.query(Translate, [TranslateResult, Recommend, NormalStatus, TranslateResult_EN, Recommend_EN, UID],
                        (error, results) => {
                            if (error) {
                                console.log(error)
                                response.status(404).send("Not Found")
                            }
                        })
                }

            }
            response.status(200).json(results.rows);
        })

    } catch (error) {
        console.log(error)
    }

}





DeleteConditionSet = (request, response) => {
    const id = parseInt(request.params.UID)
    try {
        pool.query('DELETE FROM "msCondition" WHERE "UID" = $1', [id], (error, results) => {
            if (error) {
                console.log('delect msCondition' + error)
                response.status(404).send("Not Found")
            }

            try {
                pool.query('DELETE FROM "msResultTranslate" WHERE "UID" = $1', [id], (error, results) => {
                    if (error) {
                        console.log('delect msResultTranslate' + error)

                    }
                })
            } catch (error) {
                console.log('delect msResultTranslate' + error)
            }
            //console.log(id)
            response.status(200).json(results.rows);
        })
    } catch (error) {
        response.status(404).send("Not Found")
    }

}






module.exports = {
    getCondition,
    getConditionbySearch,
    postCondition,
    putCondition,
    DeleteConditionSet
}