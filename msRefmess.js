const pool = require('./Connect')

var getmsRefmess = (request, response) => {
    pool.query('SELECT "UID" as uid, slocal, sex, lang, "Mess" mess, "SSB_STD_COMM" as "ssB_STD_COMM", "CodeHIS" as "codeHIS","CommTH" as "commTH", "CommEN" as "commEN", "Code" as code, "Category" as category, sortseq as sortseq,"Flag_Active" as "flag_Active" FROM "msRefMess" ORDER BY "UID" DESC limit 5 ', (error, results) => {
        if (error) {
         //   throw error
        }
        //console.log(results);
        response.status(200).json(results.rows);
    })
}
var getRefmessByUID = (request, response) => {
    const id = parseInt(request.params.uid)//parseInt(
    pool.query('SELECT "UID" as uid, slocal, sex, lang, "Mess" mess, "SSB_STD_COMM" as "ssB_STD_COMM", "CodeHIS" as "codeHIS","CommTH" as "commTH", "CommEN" as "commEN", "Code" as code, "Category" as category, sortseq as sortseq,"Flag_Active" as "flag_Active" FROM "msRefMess" WHERE "UID" = $1', [id], (error, results) => {
        if (error) {
      //      throw error
        }
        response.status(200).json(results.rows);
    })
}
var getRefmessByName = (request, response) => {
    const name = '%'+request.params.name+'%'//parseInt(
    pool.query('SELECT "UID" as uid, slocal, sex, lang, "Mess" mess, "SSB_STD_COMM" as "ssB_STD_COMM", "CodeHIS" as "codeHIS","CommTH" as "commTH", "CommEN" as "commEN", "Code" as code, "Category" as category, sortseq as sortseq,"Flag_Active" as "flag_Active" FROM "msRefMess" WHERE lower("slocal") like lower($1) or lower("Mess") like lower($1) limit 20', [name] , (error, results) => {
        if (error) {
       //     throw error
        }
        //console.log([name])
        response.status(200).json(results.rows);
    })
}
var createRefmess = (request, response) => {
    const {slocal,
        sex,
        lang,
        mess,
        ssB_STD_COMM,
        codeHIS,
        commTH,
        commEN,
        code,
        category,
        sortseq,
        flag_Active } = request.body
    pool.query('INSERT INTO "msRefMess" ("slocal", "sex","lang","Mess","SSB_STD_COMM","CodeHIS","CommTH","CommEN","Code","Category","sortseq","Flag_Active") VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) RETURNING "UID"',
        [slocal,sex,lang,mess,ssB_STD_COMM,codeHIS,commTH,commEN,code,category,sortseq,flag_Active], (error, results) => {
            if (error) {
          //      throw error
            }
            response.status(200).json(results.rows);
        })
}
var updateRefmess = (request, response) => {
    const id = parseInt(request.params.uid)
    const {slocal,
        sex,
        lang,
        mess,
        ssB_STD_COMM,
        codeHIS,
        commTH,
        commEN,
        code,
        category,
        sortseq,
        flag_Active,uid } = request.body
    pool.query(
        'UPDATE "msRefMess" SET "slocal"=$1, "sex"=$2, "lang"=$3, "Mess"=$4, "SSB_STD_COMM"=$5, "CodeHIS"=$6,"CommTH"=$7,"CommEN"=$8, "Code"=$9,"Category"=$10,"sortseq"=$11,"Flag_Active"=$12 WHERE "UID" = $13  RETURNING  * ',
        [slocal,sex,lang,mess,ssB_STD_COMM,codeHIS,commTH,commEN,code,category,sortseq,flag_Active,uid],
        (error, results) => {
            if (error) {
        //        throw error
            }
            response.status(200).json(id);
        }
    )
}
var deleteRefmess = (request, response) => {
    const id = parseInt(request.params.uid)
    pool.query('DELETE FROM "msRefMess" WHERE "UID" = $1 RETURNING * ' ,[id], (error, results) => {
        if (error) {
      //      throw error
        }
        //console.log(id)
        response.status(200).json(results.rows);
    })
}
module.exports = {
    getmsRefmess,
    getRefmessByUID,
    getRefmessByName,
    createRefmess,
    updateRefmess,
    deleteRefmess,
}