const pool = require('./Connect')

const getLabCode = (request, response) => {
    pool.query('SELECT * FROM "msLabCode" where "Desc" is not null ORDER BY "UID" DESC limit 5', (error, results) => {
        if (error) {
        //    throw error
        }
        response.status(200).json(results.rows);
    })
 } 
 const getLabCodeBySearch = (request, response) => {  
    try{  
        const search = "%" + request.params.search + "%" ; 
        pool.query('SELECT "UID", "Desc", "Code", "ResultFormat", "Category", "Rpt_Desc", "Rpt_NormalRang", "Rpt_Group", "Rpt_GroupRow", "Rpt_NormalRang_Female", sq, "Unit",'
        +' "FixGender", "mapExtCode", "MapCodeSSB_PYT1", "MapNameSSB_PYT1", mapcode_psh, mapname_psh, mapcode_ppch, mapname_ppch, mapcode_brh, mapname_brh,mapcode_thg '
        +'FROM "msLabCode" '
        +'WHERE lower("Desc") like  lower($1) '
        +'or   lower("Code") like  lower($1)'
        +'or  lower("ResultFormat") like  lower($1)'
        +'or  lower("Category") like  lower($1)'
        +'or  lower("mapcode_thg") like  lower($1)'
        +' ORDER BY "UID" DESC',[search], (error, results) => {   
            if (error) { 
                response.status(404).send("Not Found")
            }
         response.status(200).json(results.rows);  
        })
    }
    catch(e){
        response.status(404).send("Not Found")
    } 
 }

const postLabCode = (request , response)=>{ 
const { Category,Code, Desc,FixGender,MapCodeSSB_PYT1,MapNameSSB_PYT1,ResultFormat,Rpt_Desc,Rpt_Group,Rpt_GroupRow,Rpt_NormalRang,Rpt_NormalRang_Female,
 Unit, mapExtCode,mapcode_brh,mapcode_psh,UID,mapcode_ppch,mapname_ppch,mapname_brh,sq,mapcode_thg } = request.body ;
 (async () => {
    const client = await pool.connect()

    try {
        await client.query('BEGIN')
        const queryText = 'SELECT max("UID")+1 as uid FROM "msLabCode"'
        const res = await client.query(queryText)
        pool.query('INSERT INTO "msLabCode" ("UID","Desc", "Code","ResultFormat","Category","Rpt_Desc","Rpt_NormalRang","Rpt_Group","Rpt_GroupRow","Rpt_NormalRang_Female",sq,"Unit","FixGender","mapExtCode","MapCodeSSB_PYT1","MapNameSSB_PYT1",mapcode_psh,mapcode_ppch,mapname_ppch,mapcode_brh,mapname_brh,mapcode_thg) VALUES ($1, $2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22)  RETURNING  *',
          [res.rows[0].uid, Desc, Code,ResultFormat,Category,Rpt_Desc,Rpt_NormalRang,Rpt_Group,Rpt_GroupRow,Rpt_NormalRang_Female,sq,Unit,FixGender,mapExtCode,MapCodeSSB_PYT1,MapNameSSB_PYT1,mapcode_psh,mapcode_ppch,mapname_ppch,mapcode_brh,mapname_brh,mapcode_thg], (error, results) => {
            if (error) {
                console.log(error)
                response.status(404).send("Not Found");
            }else{
                response.status(200).json(results.rows);
            }
        })
    } catch (e) { 
        response.status(404).send("Not Found"); 
    } 
 })().catch(e => console.error(e.stack)) 
}

const putLabcode = (request , response)=>{
    const id = parseInt(request.params.uid)
    const { Category,Code, Desc,FixGender,MapCodeSSB_PYT1,MapNameSSB_PYT1,ResultFormat,Rpt_Desc,Rpt_Group,Rpt_GroupRow,
        Rpt_NormalRang,Rpt_NormalRang_Female,
        Unit, mapExtCode,mapcode_brh,mapcode_psh,UID,mapcode_ppch,mapname_ppch,mapname_brh,sq ,mapcode_thg} = request.body ; 
 
     try {
         

        pool.query(
            'UPDATE "msLabCode" SET "Desc"=$1, "Code"=$2,"ResultFormat"=$3,"Category"=$4,"Rpt_Desc"=$5,"Rpt_NormalRang"=$6,"Rpt_Group"=$7,"Rpt_GroupRow"=$8,"Rpt_NormalRang_Female"=$9,sq=$10,"Unit"=$11,"FixGender"=$12,"mapExtCode"=$13,"MapCodeSSB_PYT1"=$14,"MapNameSSB_PYT1"=$15,mapcode_psh=$16,mapcode_ppch=$17,mapname_ppch=$18,mapcode_brh=$19,mapname_brh=$20  , mapcode_thg = $21 WHERE "UID" = $22   RETURNING  *',
            [Desc, Code, ResultFormat, Category, Rpt_Desc, Rpt_NormalRang,
                Rpt_Group, Rpt_GroupRow, Rpt_NormalRang_Female, sq, Unit, FixGender,
                mapExtCode, MapCodeSSB_PYT1, MapNameSSB_PYT1, mapcode_psh,
                mapcode_ppch, mapname_ppch, mapcode_brh, mapname_brh,mapcode_thg, id],
            (error, results) => {
                if (error) { 
                    response.status(404).send("Not Found");
                }
                else {
                    response.status(200).json(results.rows);
                } 
            })

     } catch (error) {
        response.status(404).send("Not Found");
     }  

}

const deleteUser = (request, response) => {
    const id = parseInt(request.params.uid)
    let result = {}

    pool.query('DELETE FROM "msLabCode" WHERE "UID" = $1', [id], (error, results) => {
        if (error) {
            result.success=false;
        }else{
            result.success= true;
        }
        
        // response.status(200).json({"mess":2,"Description":"ลบแล้วครับ"});
        response.send(JSON.stringify(result))
    })
}


 

module.exports = {
    getLabCode,
    getLabCodeBySearch,
    postLabCode,
    putLabcode,
    deleteUser
}

