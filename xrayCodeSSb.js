const pool = require('./Connect')

var getXrayCode_ssb = (request, response) => {
    pool.query('SELECT uid, "Code", "Name", "Activity", "Category_ssb", "Category_Hseries", "Category_msResultTemplate" FROM "msXrayCode_ssb" order by uid desc limit 3 ', (error, results) => {
        if (error) {
          console.log(error)
          response.status(404).send("Not Found");
        } else{
          //   console.log(results);
            response.status(200).json(results.rows);
        }

    })
}
var getXrayCode_ssbByUID = (request, response) => {
    const id = parseInt(request.params.uid)//parseInt(
    pool.query('SELECT uid, "Code", "Name", "Activity", "Category_ssb", "Category_Hseries", "Category_msResultTemplate" FROM "msXrayCode_ssb" WHERE "uid" = $1  order by uid desc' , [id], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
          //   console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
var getXrayCode_ssbyName = (request, response) => {
    const name = '%' + request.params.name + '%'//parseInt(
    pool.query('SELECT uid, "Code", "Name", "Activity", "Category_ssb", "Category_Hseries", "Category_msResultTemplate" FROM "msXrayCode_ssb" WHERE lower("Name") like lower($1) or lower("Code") like lower($1)  order by uid desc limit 20', [name], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
const createXrayCode_ssb = (request, response) => {
    const { uid, Code, Name,Category_ssb, Category_Hseries, Category_msResultTemplate,Activity} = request.body
 pool.query('INSERT INTO "msXrayCode_ssb" ("Code", "Name", "Category_ssb", "Category_Hseries","Category_msResultTemplate" , "Activity") VALUES ($1,$2,$3,$4,$5,$6) RETURNING "uid"',
     [Code, Name,Category_ssb, Category_Hseries, Category_msResultTemplate,Activity], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })
 }
 const updateXrayCode_ssb = (request, response) => {
    const id = parseInt(request.params.uid)
    const {uid, Code, Name,Category_ssb, Category_Hseries, Category_msResultTemplate,Activity } = request.body
    pool.query(
        'UPDATE "msXrayCode_ssb" SET "Code"=$2,"Name"=$3,"Category_ssb"=$4,"Category_Hseries"=$5,"Category_msResultTemplate"=$6 ,"Activity"=$7 WHERE "uid" = $1 Returning * ',
        [uid, Code, Name,Category_ssb, Category_Hseries, Category_msResultTemplate,Activity],
        (error, results) => {
            if (error) {
                console.log(error)
                response.status(404).send("Not Found");
            }
            else{
               // console.log(results);
                response.status(200).json(results.rows);
            }
        }
    )
}
const deleteXrayCode_ssb = (request, response) => {
    const id = parseInt(request.params.uid)
    pool.query('DELETE FROM "msXrayCode_ssb" WHERE "uid" = $1',[id], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
module.exports = {
    getXrayCode_ssb,
    getXrayCode_ssbByUID,
    getXrayCode_ssbyName,
    createXrayCode_ssb,
    updateXrayCode_ssb,
    deleteXrayCode_ssb,
}