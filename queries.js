//const Pool = require('pg').Pool
//const pool = new Pool({
 //   user: 'postgres',
  //  host: '192.168.10.32',
 //   database: 'seriesdb_psv',
 //   password: 'P@ssw0rd1168',
  //  port: 5432,
//})
const pool = require('./Connect')

const getUsers = (request, response) => {
    pool.query('SELECT "UID" as uid, "Desc" as desc , "Code" as code, "ResultFormat" as "resultFormat", "Category" as category,"Rpt_Desc" as "rpt_Desc", "Rpt_NormalRang" as "rpt_NormalRang","Rpt_Group" as "rpt_Group", "Rpt_GroupRow" as "rpt_GroupRow" , "Rpt_NormalRang_Female" as "rpt_NormalRang_Female", sq as sq,"Unit" as unit, "FixGender" as "fixGender", "mapExtCode" as "mapExtCode","MapCodeSSB_PYT1" as "mapCodeSSB_PYT1", "MapNameSSB_PYT1" as "mapNameSSB_PYT1", mapcode_psh as mapcode_psh, mapname_psh as mapname_psh,mapcode_ppch as mapcode_ppch, mapname_ppch, mapcode_brh, mapname_brh FROM "msLabCode" ORDER BY "UID" ASC', (error, results) => {
        if (error) {
            response.status(404).send("Not Found");
        }
        response.status(200).json(results.rows)
    })
}
const getUserByCode = (request, response) => {
    const Code = "%" + request.params.Code + "%"//parseInt(
    pool.query('SELECT  "UID" as uid, "Desc" as desc , "Code" as code, "ResultFormat" as "resultFormat", "Category" as category,"Rpt_Desc" as "rpt_Desc", "Rpt_NormalRang" as "rpt_NormalRang","Rpt_Group" as "rpt_Group", "Rpt_GroupRow" as "rpt_GroupRow" , "Rpt_NormalRang_Female" as "rpt_NormalRang_Female", sq as sq,"Unit" as unit, "FixGender" as "fixGender", "mapExtCode" as "mapExtCode","MapCodeSSB_PYT1" as "mapCodeSSB_PYT1", "MapNameSSB_PYT1" as "mapNameSSB_PYT1", mapcode_psh as mapcode_psh, mapname_psh as mapname_psh,mapcode_ppch as mapcode_ppch, mapname_ppch, mapcode_brh, mapname_brh FROM "msLabCode" WHERE lower("Code") like lower($1) or lower("Desc") like lower($1) limit 20', [Code], (error, results) => {
        if (error) {
            response.status(404).send("Not Found");
        }
        response.status(200).json(results.rows)
    })
}
const getUserById = (request, response) => {
    const uid = request.params.uid//parseInt(
    pool.query('SELECT "UID" as uid, "Desc" as desc , "Code" as code, "ResultFormat" as "resultFormat", "Category" as category,"Rpt_Desc" as "rpt_Desc", "Rpt_NormalRang" as "rpt_NormalRang","Rpt_Group" as "rpt_Group", "Rpt_GroupRow" as "rpt_GroupRow" , "Rpt_NormalRang_Female" as "rpt_NormalRang_Female", sq as sq,"Unit" as unit, "FixGender" as "fixGender", "mapExtCode" as "mapExtCode","MapCodeSSB_PYT1" as "mapCodeSSB_PYT1", "MapNameSSB_PYT1" as "mapNameSSB_PYT1", mapcode_psh as mapcode_psh, mapname_psh as mapname_psh,mapcode_ppch as mapcode_ppch, mapname_ppch, mapcode_brh, mapname_brh FROM "msLabCode" WHERE "UID" = $1 ', [uid], (error, results) => {
        if (error) {
            response.status(404).send("Not Found");
            //throw error
        }
        response.status(200).json(results.rows)
    })
}
const getLabcode = (request, response) => {
    pool.query('SELECT "UID" FROM "msLabCode" ORDER BY "UID" DESC LIMIT 1', (error, results) => {
        if (error) {
         response.status(404).send("Not Found");
             //  throw error
        }
        response.status(200).json(results.rows[0]["UID"] + 1);
    })
}
const createUser = (request, response) => {

    (async () => {
        const { UID,Desc, Code,ResultFormat,Category,Rpt_Desc,Rpt_NormalRang,Rpt_Group,Rpt_GroupRow,
            Rpt_NormalRang_Female,sq,Unit,FixGender,mapExtCode,MapCodeSSB_PYT1,MapNameSSB_PYT1,
            mapcode_psh,mapcode_ppch,mapname_ppch,mapcode_brh,mapname_brh } = request.body
        const client = await pool.connect()
        try {
            await client.query('BEGIN')
            const queryText = 'SELECT max("UID")+1 as uid FROM "msLabCode"'
            const res = await client.query(queryText)
            console.log(res.rows[0].uid)
            const insertPhotoText = 'INSERT INTO "msLabCode" ("UID","Desc", "Code","ResultFormat","Category","Rpt_Desc","Rpt_NormalRang","Rpt_Group","Rpt_GroupRow","Rpt_NormalRang_Female",sq,"Unit","FixGender","mapExtCode","MapCodeSSB_PYT1","MapNameSSB_PYT1",mapcode_psh,mapcode_ppch,mapname_ppch,mapcode_brh,mapname_brh) VALUES ($1, $2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21)'
            const insertPhotoValues = [res.rows[0].uid, Desc, Code,ResultFormat,Category,Rpt_Desc,Rpt_NormalRang,Rpt_Group,Rpt_GroupRow,Rpt_NormalRang_Female,sq,Unit,FixGender,mapExtCode,MapCodeSSB_PYT1,MapNameSSB_PYT1,mapcode_psh,mapcode_ppch,mapname_ppch,mapcode_brh,mapname_brh]
            await client.query(insertPhotoText, insertPhotoValues)

            
            await client.query('COMMIT')
            console.log(results.rows)
            response.status(200).json(results.rows);
        } catch (e) {
            await client.query('ROLLBACK')
            response.status(404).send("Not Found");
            //throw e
        } finally {
            client.release()
        }
    })().catch(e => console.error(e.stack))
}


const updateUser = (request, response) => {
    const id = parseInt(request.params.uid)
    const { desc, code, resultFormat, category, rpt_Desc, rpt_NormalRang,
        rpt_Group, rpt_GroupRow, rpt_NormalRang_Female, sq, unit, fixGender,
        mapExtCode, mapCodeSSB_PYT1, mapNameSSB_PYT1, mapcode_psh,
        mapcode_ppch, mapname_ppch, mapcode_brh, mapname_brh, uid } = request.body

    pool.query(
        'UPDATE "msLabCode" SET "Desc"=$1, "Code"=$2,"ResultFormat"=$3,"Category"=$4,"Rpt_Desc"=$5,"Rpt_NormalRang"=$6,"Rpt_Group"=$7,"Rpt_GroupRow"=$8,"Rpt_NormalRang_Female"=$9,sq=$10,"Unit"=$11,"FixGender"=$12,"mapExtCode"=$13,"MapCodeSSB_PYT1"=$14,"MapNameSSB_PYT1"=$15,mapcode_psh=$16,mapcode_ppch=$17,mapname_ppch=$18,mapcode_brh=$19,mapname_brh=$20 WHERE "UID" = $21',
        [desc, code, resultFormat, category, rpt_Desc, rpt_NormalRang,
            rpt_Group, rpt_GroupRow, rpt_NormalRang_Female, sq, unit, fixGender,
            mapExtCode, mapCodeSSB_PYT1, mapNameSSB_PYT1, mapcode_psh,
            mapcode_ppch, mapname_ppch, mapcode_brh, mapname_brh, id],
        (error, results) => {
            if (error) {
                //throw error
                response.status(404).send("Not Found");
            }
            else {
                response.json({ status: 200 });
            }

        }

    )
}
const deleteUser = (request, response) => {
    const id = parseInt(request.params.uid)
    let result = {}

    pool.query('DELETE FROM "msLabCode" WHERE "UID" = $1', [id], (error, results) => {
        if (error) {
            result.success=false;
        }else{
            result.success= true;
        }
        
        // response.status(200).json({"mess":2,"Description":"ลบแล้วครับ"});
        response.send(JSON.stringify(result))
    })
}

module.exports = {
    getUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser,
    getLabcode,
    getUserByCode,
}