


const pool = require('./Connect')

var getpatientHistorybyid = (request, response) => {
    const id = parseInt(request.params.UID)//parseInt(
    pool.query('SELECT "UID", "trPatientUID", "Recommend", "SmokingID", "SmokingVolume", "SmokingYear", "SmokingMonth", "SmokingRemark", "AlcoholicDrinkID", "AlcoholicVolume", "AlcoholicDrinkYear", "AlcoholicDrinkMonth", "AlcoholicRemark", "ExerciseID", "ExerciseRemark", "HabitID", "HabitRemark", "WkRisk1", "WkRisk1Result", "WkRisk2", "WkRisk2Result", "WkRisk3", "WkRisk3Result", "WkRisk4", "WkRisk4Result", "WkRisk5", "WkRisk5Result", "PresentIllness", "PresentDisease", "Medicine", "Allergy", "PassDisease", "PassSurgery", "PassOther", "IllFather", "IllMother", "IllChild", "FamilyOther", "IsFit", "IsPreg", "LastMens", "FamilyHistoryRisk", "CholesterolRisk", "DiabeticRisk", "ObesityRisk", "SmokingRisk", "LifeStyleRisk", "OffShoreInfoID", "OffShoreComment1ID", "OffShoreComment2ID", "OffShoreCommentWeek", "OffShoreFollowUnit", "OffShoreOthers", "Language", "StatusFlag", "CUser", "CWhen", "MUser", "MWhen", "AddressCompany", "AddressOther", "AddressHouseNo", "AddressVillageNo", "Road", "SubDistrict", "District", "Province", "ZipCode", "Phone", "Email", "PersonalHistoryInherite", "PersonalHistoryChange", "RecordDate", "PersonalHistoryPresentIllness", "PersonalHistoryCurrentMedication", "PersonalHistoryAllergy", "PersonalHistoryAccident", "PersonalHistorySurgery", "PersonalHistoryAdmission", "SmokingNone", "SmokingQuit", "SmokingYears", "Smoking", "SmokingVolumes", "SmokingLong", "AlcoholNone", "AlcoholQuit", "AlcoholYears", "Alcohol", "AlcoholVolume", "Exercise", "ExerciseType", "FamilyHistoryInherite", "FamilyHistoryChange", "FamilyHistoryDiabetes", "FamilyHistoryDiabetesDetail", "FamilyHistoryHeartDiesase", "FamilyHistoryHeartDiesaseDetail", "FamilyHistoryDyslipidemia", "FamilyHistoryDyslipidemiaDetail", "FamilyHistoryAnemia", "FamilyHistoryAnemiaDetail", "FamilyHistoryHypertension", "FamilyHistoryHypertensionDetail", "FamilyHistoryCancer", "FamilyHistoryCancerDetail", "FamilyHistoryHepatitis", "FamilyHistoryHepatitisDetail", "FamilyHistoryOther", "FamilyHistoryOtherDetail", "MenstuationStart", "MenstuationEnd", "MenstuationLong", "UserSign", "AddressAlley", "MenstuationLongUnit", "FamilyHistoryTuberculosis", "FamilyHistoryAllergy", "SpecialCheckByJobDesc", "FamilyHistoryTuberculosisDetail", "FamilyHistoryAllergyDetail", "Immunity", "ImmunityDetail", "Drugs", "HistoryOther", "PHComment", "SmokingOther", "MHAsthma", "MHHypertension", "MHHemoptysis", "MHHeart", "MHDiabetes", "MHJaundice", "MHEpilepsy", "MHVeneral", "MHAcquired", "MHEdema", "MHYaws", "MHOtorrhea", "MHHernia", "MHHEmorrhoid", "MHAccident", "MHFractures", "MHSurgical", "MHMalaria", "MHLMP", "MHDetail", "SmokingQuitOther", "AlcoholQuitOther", "SmokingQuitDetail2" FROM public."trPatientHistory"  WHERE "trPatientUID" = $1  order by "trPatientUID" desc' , [id], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
          //   console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
module.exports = {
    getpatientHistorybyid,

}