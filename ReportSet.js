const pool = require('./Connect')

var getReport = (request, response) => {
    pool.query('SELECT "UID", "ReportName", "Type", "Status", "CDate", "Sort", "ReportShowName", path FROM public."TR_ReportSum" order by "UID" desc limit 3', (error, results) => {
        if (error) {
          console.log(error)
          response.status(404).send("Not Found");
        } else{
          //   console.log(results);
            response.status(200).json(results.rows);
        }

    })
}
var getReportbyid = (request, response) => {
    const id = parseInt(request.params.UID)//parseInt(
    pool.query('SELECT "UID", "ReportName", "Type", "Status", "CDate", "Sort", "ReportShowName", path FROM public."TR_ReportSum" WHERE "UID" = $1  order by "UID" desc' , [id], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
          //   console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
var getReportbySearch = (request, response) => {
    const name = '%' + request.params.search + '%'//parseInt(
    pool.query('SELECT "UID", "ReportName", "Type", "Status", "CDate", "Sort", "ReportShowName", path FROM public."TR_ReportSum" WHERE lower("ReportName") like lower($1) or lower("ReportShowName") like lower($1)  order by "UID" desc limit 20', [name], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
const postReport = (request, response) => { 
    // const {ReportName, Type,Status, CDate, Sort,ReportShowName,path,UID} = request.body;
    const {ReportName, Type,Status,  Sort,ReportShowName,UID} = request.body;
 var CDate ="now()" ;
 var path ="" ; 
    const queryText = 'SELECT max("UID")+1 as uid FROM "TR_ReportSum"' ;
     pool.query(queryText,(error,res)=>{
        if(error){
            console.log(error)
        } 
 pool.query('INSERT INTO public."TR_ReportSum"( "UID","ReportName", "Type", "Status", "CDate", "Sort", "ReportShowName",path) VALUES ($1,$2,$3,$4,$5,$6,$7,$8) RETURNING * ',
     [ res.rows[0].uid,ReportName, Type,Status, CDate, Sort,ReportShowName,path], (error, results) => {
        if (error) 
        {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else
        {
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })
}) 
 }




 const putReport = (request, response) => {
    const id = parseInt(request.params.UID)
    const {UID,ReportName,Type, Status, CDate,Sort,ReportShowName } = request.body
    pool.query(
        'UPDATE public."TR_ReportSum" SET "UID"=$1, "ReportName"=$2, "Type"=$3, "Status"=$4, "CDate"=$5, "Sort"=$6, "ReportShowName"=$7 WHERE "UID" = $1 Returning * ',
        [UID, ReportName, Type,Status, CDate, Sort,ReportShowName],
        (error, results) => {
            if (error) {
                console.log(error)
                response.status(404).send("Not Found");
            }
            else{
               // console.log(results);
                response.status(200).json(results.rows);
            }
        }
    )
}
const deleteReport = (request, response) => {
    const id = parseInt(request.params.UID)
    pool.query('DELETE FROM "TR_ReportSum" WHERE "UID" = $1',[id], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
module.exports = {
    getReport,
    getReportbyid,
    getReportbySearch,
    postReport,
    putReport,
    deleteReport,
}