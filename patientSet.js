const pool = require('./Connect')

var getPatient = (request, response) => {
    pool.query('SELECT "UID", "HN", "EN", "Prename", "Forename", "Surname", "Age", "Sex", "MaritalStatus", "DOB", "Email", "RegisType", "Telephone", "Mobile", "CheckupDate", "LabNo", "NO", "ProChkList", "GroupBook", "Company", "EmpID", "Position", "Shift", "Job", "Dept", "Section", "Line", "Division", "BusUnit", "BusDiv", "Location", "Language", "StatusFlag", "CUser", "CWhen", "MUser", "MWhen", "Address", "CARDID", "ARCode", "RefNo", "RequestNo", "ResultStatus", "CHECKUPMETHOD", "ProgramDesc", "ARName", "CompanyCode", "UIDLanguage", "NationalityCode", "Nationality", "ChekRight", "ChekStation", "RightData", "CountCheckList", "CustomerDept", "CustomerPosition", "CustomerDeptname", "CustomerPositionname", "Prename_Eng", "Forename_Eng", "Surname_Eng", "PKCode", "VSSTS", "StatusLoaded", "DateRegisByLoad", "HispatientUID", "HispatientvisitUID", "HisVisitCareProvider", "EmpPinCode", "EmpAmphur", "EmpProvince", "BatchNo", "HisVisitCareProviderEnglishName", "PassportID", "AddressEN", "AddressNumber", "HisVisitCareProviderLicenseID", "MergeToPatientUID", "AgeDetail", "AddressOther", "CheckupNo" FROM public."trPatient" order by "UID" desc limit 5', (error, results) => {
        if (error) {
          console.log(error)
          response.status(404).send("Not Found");
        } else{
          //   console.log(results);
            response.status(200).json(results.rows);
        }

    })
}
var getPatientbyid = (request, response) => {
    const id = parseInt(request.params.UID)//parseInt(
    pool.query('SELECT "UID", "HN", "EN", "Prename", "Forename", "Surname", "Age", "Sex", "MaritalStatus", "DOB", "Email", "RegisType", "Telephone", "Mobile", "CheckupDate", "LabNo", "NO", "ProChkList", "GroupBook", "Company", "EmpID", "Position", "Shift", "Job", "Dept", "Section", "Line", "Division", "BusUnit", "BusDiv", "Location", "Language", "StatusFlag", "CUser", "CWhen", "MUser", "MWhen", "Address", "CARDID", "ARCode", "RefNo", "RequestNo", "ResultStatus", "CHECKUPMETHOD", "ProgramDesc", "ARName", "CompanyCode", "UIDLanguage", "NationalityCode", "Nationality", "ChekRight", "ChekStation", "RightData", "CountCheckList", "CustomerDept", "CustomerPosition", "CustomerDeptname", "CustomerPositionname", "Prename_Eng", "Forename_Eng", "Surname_Eng", "PKCode", "VSSTS", "StatusLoaded", "DateRegisByLoad", "HispatientUID", "HispatientvisitUID", "HisVisitCareProvider", "EmpPinCode", "EmpAmphur", "EmpProvince", "BatchNo", "HisVisitCareProviderEnglishName", "PassportID", "AddressEN", "AddressNumber", "HisVisitCareProviderLicenseID", "MergeToPatientUID", "AgeDetail", "AddressOther", "CheckupNo" FROM public."trPatient" WHERE "UID" = $1  order by "UID" desc' , [id], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
          //   console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
var getPatientbySearch = (request, response) => {
    const name = '%' + request.params.search + '%'//parseInt(
    pool.query('SELECT "UID", "HN", "EN", "Prename", "Forename", "Surname", "Age", "Sex", "MaritalStatus", "DOB", "Email", "RegisType", "Telephone", "Mobile", "CheckupDate", "LabNo", "NO", "ProChkList", "GroupBook", "Company", "EmpID", "Position", "Shift", "Job", "Dept", "Section", "Line", "Division", "BusUnit", "BusDiv", "Location", "Language", "StatusFlag", "CUser", "CWhen", "MUser", "MWhen", "Address", "CARDID", "ARCode", "RefNo", "RequestNo", "ResultStatus", "CHECKUPMETHOD", "ProgramDesc", "ARName", "CompanyCode", "UIDLanguage", "NationalityCode", "Nationality", "ChekRight", "ChekStation", "RightData", "CountCheckList", "CustomerDept", "CustomerPosition", "CustomerDeptname", "CustomerPositionname", "Prename_Eng", "Forename_Eng", "Surname_Eng", "PKCode", "VSSTS", "StatusLoaded", "DateRegisByLoad", "HispatientUID", "HispatientvisitUID", "HisVisitCareProvider", "EmpPinCode", "EmpAmphur", "EmpProvince", "BatchNo", "HisVisitCareProviderEnglishName", "PassportID", "AddressEN", "AddressNumber", "HisVisitCareProviderLicenseID", "MergeToPatientUID", "AgeDetail", "AddressOther", "CheckupNo" FROM public."trPatient" WHERE lower("HN") like lower($1) or lower("Forename") like lower($1)  order by "UID" desc limit 20', [name], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })
} 
module.exports = {
    getPatient,
    getPatientbyid,
    getPatientbySearch,
    // postPatient,
    // putPatient,
    // deletePatient,
}