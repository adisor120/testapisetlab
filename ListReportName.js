const pool = require('./Connect')

var getListReportName = (request, response) => {
    pool.query('SELECT uid, trname, trid, nameshow, status 	FROM public.listtrreportname order by "uid" desc', (error, results) => {
        if (error) {
          console.log(error)
          response.status(404).send("Not Found");
        } else{
          //   console.log(results);
            response.status(200).json(results.rows);
        }

    })
}
var getListReportNamebyid = (request, response) => {
    const id = parseInt(request.params.UID)//parseInt(
    pool.query('SELECT uid, trname, trid, nameshow, status 	FROM public.listtrreportname WHERE "uid" = $1  order by "uid" desc  limit 20', [id], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
          //   console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
var getListReportNamebySearch = (request, response) => {
    const name = '%' + request.params.name + '%'//parseInt(
        console.log(name)
    pool.query('SELECT uid, trname, trid, nameshow, status 	FROM public.listtrreportname WHERE lower("trname") like lower($1) or lower("nameshow") like lower($1)  order by "uid" desc limit 20', [name], (error, results) => {
       console.log(error)
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
const postListReportName = (request, response) => { 
    // const {ReportName, Type,Status, CDate, Sort,ReportShowName,path,UID} = request.body;
    const {uid, trname, trid, nameshow, status} = request.body;
 var CDate ="now()" ;
 var path ="" ; 
    const queryText = 'SELECT max("UID")+1 as uid FROM "TR_ReportSum"' ;
     pool.query(queryText,(error,res)=>{
        if(error){
            console.log(error)
        } 
 pool.query('INSERT INTO public.listtrreportname( trname, trid, nameshow, status) VALUES  ($1,$2,$3,$4) RETURNING * ',
     [ trname, trid,nameshow, status], (error, results) => {
        if (error) 
        {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else
        {
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })
}) 
 }




 const putListReportName = (request, response) => {
    const id = parseInt(request.params.UID)
    const {uid, trname, trid, nameshow, status}  = request.body
    pool.query(
        'UPDATE public.listtrreportname SET uid=$1, trname=$2, trid=$3, nameshow=$4, status=$5 WHERE "uid" = $1 Returning * ',
        [uid, trname, trid, nameshow, status],
        (error, results) => {
            if (error) {
                console.log(error)
                response.status(404).send("Not Found");
            }
            else{
               // console.log(results);
                response.status(200).json(results.rows);
            }
        }
    )
}
const deleteListReportName = (request, response) => {
    const id = parseInt(request.params.uid)
    pool.query('DELETE FROM "listtrreportname" WHERE "uid" = $1',[id], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
module.exports = {
    getListReportName,
    getListReportNamebyid,
    getListReportNamebySearch,
    postListReportName,
    putListReportName,
    deleteListReportName,
}