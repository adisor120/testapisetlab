const pool = require('./Connect')

var getReportName = (request, response) => {
    pool.query('SELECT uid, reportnameth, reportnameen, reportcatagory, sort, reportfilename, map_packagecode 	FROM public.ms_reportname order by uid desc limit 5 ', (error, results) => {
        if (error) {
          console.log(error)
          response.status(404).send("Not Found");
        } else{
          //   console.log(results);
            response.status(200).json(results.rows);
        }

    })
}

var getReportNameByUID = (request, response) => {
    const id = parseInt(request.params.uid)//parseInt(
    pool.query('SELECT uid, reportnameth, reportnameen, reportcatagory, sort, reportfilename, map_packagecode 	FROM public.ms_reportname WHERE "uid" = $1  order by uid desc' , [id], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
          //   console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
var getReportNameyName = (request, response) => {
    const name = '%' + request.params.name + '%'//parseInt(
    pool.query('SELECT uid, reportnameth, reportnameen, reportcatagory, sort, reportfilename, map_packagecode 	FROM public.ms_reportname WHERE lower("reportnameth") like lower($1) or lower("reportcatagory") like lower($1)  order by uid desc limit 20', [name], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
const createReportName = (request, response) => {
    const { uid, reportnameth, reportnameen,reportcatagory, sort, reportfilename,map_packagecode} = request.body 

    const queryText = 'SELECT max("uid")+1 as uid FROM "ms_reportname"' ;
    pool.query(queryText,(error,res)=>{
       if(error){
           console.log(error)
       }


 pool.query('INSERT INTO public.ms_reportname ( uid, reportnameth, reportnameen, reportcatagory, sort, reportfilename, map_packagecode) VALUES ($1,$2,$3,$4,$5,$6,$7) RETURNING "uid"',
     [res.rows[0].uid, reportnameth,reportnameen, reportcatagory, sort,reportfilename,map_packagecode], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })

})
 }
 const updateReportName = (request, response) => {
    const id = parseInt(request.params.uid)
    const { uid, reportnameth, reportnameen,reportcatagory, sort, reportfilename,map_packagecode} = request.body 
    pool.query(
        'UPDATE "ms_reportname" SET "reportnameth"=$2,"reportnameen"=$3,"reportcatagory"=$4,"sort"=$5,"reportfilename"=$6 ,"map_packagecode"=$7 WHERE "uid" = $1 Returning * ',
        [uid, reportnameth, reportnameen,reportcatagory, sort, reportfilename,map_packagecode],
        (error, results) => {
            if (error) {
                console.log(error)
                response.status(404).send("Not Found");
            }
            else{
               // console.log(results);
                response.status(200).json(results.rows);
            }
        }
    )
}
const deleteReportName = (request, response) => {
    const id = parseInt(request.params.uid)
    pool.query('DELETE FROM "ms_reportname" WHERE "uid" = $1',[id], (error, results) => {
        if (error) {
            console.log(error)
            response.status(404).send("Not Found");
        }
        else{
           //  console.log(results);
            response.status(200).json(results.rows);
        }
    })
}
module.exports = {
    getReportName,
    getReportNameByUID,
    getReportNameyName,
    createReportName,
    updateReportName,
    deleteReportName,
}