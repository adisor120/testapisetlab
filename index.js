const express = require('express')
const bodyParser = require('body-parser')
const db = require('./queries')
const dbCon = require('./ConditionController')
const dbResult = require('./msResultTranslate')
const dbRefmees= require('./msRefmess')
const dbXray= require('./xrayCodeSSb')
const dbResultTemplate= require('./msResultTemplate')
const LabCode= require('./LabCode')
const ConditionSet= require('./ConditionSet')
const ReportSet= require('./ReportSet')
const PatientSet = require('./patientSet')
const vitalsign = require('./vitalsign')
const patienthistory = require('./patienthistory')
const ReportNameSet = require('./ReportNameSet')
const ListReportName = require('./ListReportName')
const app = express()
const port = 8222

app.use(bodyParser.json())
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
)
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

 

//msCondition
  app.get('/api/Condition', dbCon.getCondition)
  app.get('/api/Condition/:Code', dbCon.getConditionByCode)
  app.post('/api/Condition', dbCon.createCondition)
 app.put('/api/Condition/:uid', dbCon.updateCondition)
  app.delete('/api/Condition/:uid', dbCon.deleteCondition)

//dbResult
app.get('/api/ResultTranslate', dbResult.getResultTranslate)
app.get('/api/ResultTranslate/:uid', dbResult.getResultTranslateByUID)
app.post('/api/ResultTranslate', dbResult.createResultTranslate)
app.put('/api/ResultTranslate/:uid', dbResult.updateResultTranslate)
app.delete('/api/ResultTranslate/:uid', dbResult.deleteResultTranslate)
app.get('/api/ResultTranslate/search/:name', dbResult.getResultTranslateByName)

//dbRefmess
app.get('/api/msRefmess/', dbRefmees.getmsRefmess)
app.get('/api/msRefmess/:uid', dbRefmees.getRefmessByUID)
app.get('/api/msRefmess/search/:name', dbRefmees.getRefmessByName)
app.post('/api/msRefmess/', dbRefmees.createRefmess)
app.put('/api/msRefmess/:uid', dbRefmees.updateRefmess)
app.delete('/api/msRefmess/:uid', dbRefmees.deleteRefmess)

//dbResultxray
app.get('/api/XrayCode/', dbXray.getXrayCode_ssb)
app.get('/api/XrayCode/:uid', dbXray.getXrayCode_ssbByUID)
app.get('/api/XrayCode/search/:name', dbXray.getXrayCode_ssbyName)
app.post('/api/XrayCode/', dbXray.createXrayCode_ssb)
app.put('/api/XrayCode/:uid', dbXray.updateXrayCode_ssb)
app.delete('/api/XrayCode/:uid', dbXray.deleteXrayCode_ssb)

//dbResult
app.get('/api/ResultTemplate', dbResultTemplate.getResultTemplate)
app.get('/api/ResultTemplate/:uid', dbResultTemplate.getResultTemplateByUID)
app.post('/api/ResultTemplate', dbResultTemplate.createResultTemplate)
app.put('/api/ResultTemplate/:uid', dbResultTemplate.updateResultTemplate)
app.delete('/api/ResultTemplate/:uid', dbResultTemplate.deleteResultTemplate)
app.get('/api/ResultTemplateCategory', dbResultTemplate.getCategorybyResultTemplate)
app.get('/api/ResultTemplate/search/:name', dbResultTemplate.getResultTemplateByname)
app.post('/api/searchResultTemplate', dbResultTemplate.postResultTemplateByDescription)


//LabCode by Testing
app.get('/api/LabCodes/',LabCode.getLabCode )
app.get('/api/LabCodes/:search',LabCode.getLabCodeBySearch )
app.post('/api/LabCodes/',LabCode.postLabCode )
app.put('/api/LabCodes/:uid',LabCode.putLabcode )
app.delete('/api/LabCodes/:uid',LabCode.deleteUser )

//ConditionSte
app.get('/api/Condotionset/',ConditionSet.getCondition)
app.get('/api/Condotionset/:search',ConditionSet.getConditionbySearch)
app.post('/api/Condotionset/',ConditionSet.postCondition)
app.put('/api/Condotionset/:UID',ConditionSet.putCondition)
app.delete('/api/Condotionset/:UID',ConditionSet.DeleteConditionSet)
//ReportSet
app.get('/api/Report/',ReportSet.getReport)
app.get('/api/Report/',ReportSet.getReportbyid)
app.get('/api/Report/search/:search',ReportSet.getReportbySearch)
app.post('/api/Report/',ReportSet.postReport)
app.put('/api/Report/:UID',ReportSet.putReport)
app.delete('/api/Report/:UID',ReportSet.deleteReport)

//PatientSet
app.get('/api/Patient/',PatientSet.getPatient)
app.get('/api/Patient/:UID',PatientSet.getPatientbyid)
app.get('/api/Patient/search/:search',PatientSet.getPatientbySearch)


//vitalsign
app.get('/api/vitalsign/:UID',vitalsign.getvitasignbyid)


//PatientHistory
app.get('/api/patienthistory/:UID',patienthistory.getpatientHistorybyid)



//ReportName 
app.get('/api/ReportName/', ReportNameSet.getReportName)
app.get('/api/ReportName/:uid', ReportNameSet.getReportNameByUID)
app.get('/api/ReportName/search/:name', ReportNameSet.getReportNameyName)
app.post('/api/ReportName/', ReportNameSet.createReportName)
app.put('/api/ReportName/:uid', ReportNameSet.updateReportName)
app.delete('/api/ReportName/:uid', ReportNameSet.deleteReportName)

//ListReportName
app.get('/api/ListReportName/', ListReportName.getListReportName)
app.get('/api/ListReportName/:uid', ListReportName.getListReportNamebyid)
app.get('/api/ListReportName/search/:name', ListReportName.getListReportNamebySearch)
app.post('/api/ListReportName/', ListReportName.postListReportName)
app.put('/api/ListReportName/:uid', ListReportName.putListReportName)
app.delete('/api/ListReportName/:uid', ListReportName.deleteListReportName)


app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})